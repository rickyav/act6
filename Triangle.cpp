#include "Triangle.h"
#include "Point.h"
#include <math.h>


Triangle::Triangle()
{

}

Triangle::~Triangle()
{
}

Triangle::Triangle(Point p1, Point p2, Point p3)
{
	vertex1 = p1;
	vertex2 = p2;
	vertex3 = p3;
}

double Triangle::getPerimeter()
{
	return vertex1.getDistance(vertex2)+vertex2.getDistance(vertex3)+vertex1.getDistance(vertex3);
}


double Triangle::getArea()
{
	return sqrt(pow((vertex1.getX() * vertex2.getY() + vertex2.getX() * vertex3.getY() + vertex3.getX() * vertex1.getY())-(vertex2.getX() * vertex1.getY() + vertex3.getX() * vertex2.getY() + vertex1.getX() * vertex3.getY()),2))/2;
}
