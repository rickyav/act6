#pragma once
class Point
{
private:
	double x;
	double y;
public:
	Point(double, double);
	Point();
	~Point();
	double getX();
	double getY();
	void setX(double);
	void setY(double);
	double getDistance(Point);
};

