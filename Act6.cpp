
#include <iostream>
#include "Point.h"
#include "Triangle.h"
using namespace std;

int main()
{
    Point punto1(0, 0); 
    Point punto2(50, 30);
    Point punto3(25, 10);
   

    Triangle miTriangulo1(punto1, punto2, punto3);

    // Plan de pruebas de metodos
    cout << "El perimetro del rectangulo es " << miTriangulo1.getPerimeter();
    cout << "\nEl area es " << miTriangulo1.getArea() << "\n";
    cout << "\nLos puntos de la primera coordenada son " << punto1.getX() << "," << punto1.getY();
    cout << "\nLos puntos de la segunda coordenada son " << punto2.getX() << "," << punto2.getY();
    cout << "\nLos puntos de la tercera coordenada son " << punto3.getX() << "," << punto3.getY() << "\n";
}

