#pragma once
#include "Point.h"
class Triangle
{
private:
	Point vertex1;
	Point vertex2;
	Point vertex3;

public:
	Triangle();
	~Triangle();
	Triangle(Point, Point, Point);
	double getPerimeter();
	double getArea();
};

