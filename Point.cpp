#include "Point.h"
#include <math.h>
using namespace std;
Point::Point(double x1, double y1)
{
	x = x1;
	y = y1;
}

Point::Point()
{
	x = 0;
	y = 0;
}

Point::~Point()
{
}

double Point::getX()
{
	return x;
}

double Point::getY()
{
	return y;
}

void Point::setX(double x1)
{
	x = x1;
}

void Point::setY(double y1)
{
	y = y1;
}

double Point::getDistance(Point p1)
{
	return sqrt(pow(p1.x - x, 2) + pow(p1.y - y, 2));
}
